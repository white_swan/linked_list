/**
 * Created by uellee on 12.03.16.
 */
public class DummyLinkedList <E> {

    private int mMaxSize;
    private int mSize;
    private DummyNode<E> mRoot = new DummyNode<E>(null,null,null);

    private static class DummyNode <E> {
        E value;
        DummyNode<E> next;
        DummyNode<E> prev;

        public DummyNode(DummyNode<E> prev, E element, DummyNode<E> next) {
            this.prev = prev;
            this.value = element;
            this.next = next;
        }

        public DummyNode(DummyNode<E> dummy) {
            this.prev = dummy.prev;
            this.value = dummy.value;
            this.next = dummy.next;
        }
    }

    public DummyLinkedList(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Размер может быть только положительным числом");
        }
        this.mMaxSize = size;
    }

    public void add(E value) throws FullDummyListException {
        if (mSize == mMaxSize) {
            throw new FullDummyListException("слишком много элементов");
        }
        if (mRoot.value == null) {
            mRoot.value = value;
            mSize = 1;
            return;
        }
        DummyNode<E> current = mRoot;
        while (current.next != null) current = current.next;
        mSize++;
    }

    public E get(int index) {
        DummyNode<E> current = mRoot;
        for(int i = 0; i < index; ++i) {
            current = current.next;
        }

        return current.value;
    }
}
