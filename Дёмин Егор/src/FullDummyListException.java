/**
 * Created by uellee on 12.03.16.
 */
public class FullDummyListException extends Exception{
    public FullDummyListException(String s) {
        super(s);
    }
}
